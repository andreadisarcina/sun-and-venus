# Sun and Venus


The script is able to make a plot of the figure described by the Sun and Venus on the plane of the sky during 2018 as observed in Padua each day at 10h UT. 

The file VENUSandSUN.pdf contains the output.

The script uses the function OEtoSTATE to convert the orbit elements into the Cartesian dynamical state.
