import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from OEtoSTATE import *

#initial date (1st January 2018), universal time in hours, longitude and latitude in degrees and decimals
day=1
month=1
year=2018
UThour=10.
longitude=+11.87167         # + sign for East longitude, - sign for West longitude
latitude=+45.4              # + sign for Nord latitude, - sign for Sud latitude

#conversion of Gregorian date in Julian day (at 12:00 UT)
def julian(d,m,y):
	temp1=m-14
	temp2=d-32075+int(1461*(y+4800+int(temp1/12))/4)
	temp3=int(temp1/12)*12
	temp4=int(y+4900+int(temp1/12))/100
	jd=temp2+int(367*(m-2-temp3)/12)-int(3*temp4/4)
	return jd
jul=julian(day,month,year)            #corresponding Julian day
fraction=(UThour-12.)/24.			  #fraction of the day corresponding to UThour
t0=jul+fraction                       #initial Julian date
JD=[t0]
if(year%4==0.):                       #leap year    
	n=366
else:                                 #not leap year
	n=365
while(len(JD)<n):                   
	JD.append(JD[-1]+1.)
JD=np.array(JD)
#now JD contains all Julian dates corresponding to every day in 2018 at 10:00 UT
T=(JD-2451545.)/36525           #time in Julian centuries

#Orbital elements coefficients (in the mean ecliptic and equinox of date reference system):
Venus=np.array([[0.723329820,0.,0.,0.],                                         #a
				[0.00677192,-0.000047765,+0.0000000981,+0.00000000046],         #e
				[3.394662,+0.0010037,-0.00000088,-0.000000007],                 #i
				[76.679920,+0.9011206,+0.00040618,-0.000000093],                #Omega (W)
				[131.563703,+1.4022288,-0.00107618,-0.000005678],               #w tilde (pi)
				[181.979801,+58519.2130302,+0.00031014,+0.000000015]])          #L 

Earth=np.array([[1.000001018,0.,0.,0.],                                         #a
	            [0.01670863,-0.000042037,-0.0000001267,+0.00000000014],         #e
	            [0.,0.,0.,0.],                                                  #i
	            [0.,0.,0.,0.],                                                  #Omega (W)
	            [102.937348,+1.7195366,+0.00045688,-0.000000018],               #w tilde (pi)
				[100.466457,+36000.7698278,+0.00030322,+0.000000020]])          #L
#a is in AU, while i,W,pi,L are in deg
#gravitational parameter of the Sun (neglecting the mass of all the planets) in km^3/s^2
mu=1.3271244e11

#radius of spherical Earth in km:
Radius=6378.137
#conversion of longitude and latitude in rad:
longitude=longitude/180.*np.pi
latitude=latitude/180.*np.pi

#loop through time instants:
for i in range(len(JD)):

	#orbital elements calculation for i-th time instant
	OEVenus=Venus[:,0]+Venus[:,1]*T[i]+Venus[:,2]*T[i]**2.+Venus[:,3]*T[i]**3.
	OEEarth=Earth[:,0]+Earth[:,1]*T[i]+Earth[:,2]*T[i]**2.+Earth[:,3]*T[i]**3.

	#conversion of orbital elements at i-th time instant to dynamical states (positions in km and velocities in km/s) of the plantes at the same time instant relative to the heliocentric ecliptic reference system
	StateVenusHelioEclip=oetostate(OEVenus[0],OEVenus[1],OEVenus[2],OEVenus[3],OEVenus[4],OEVenus[5],mu)
	StateEarthHelioEclip=oetostate(OEEarth[0],OEEarth[1],OEEarth[2],OEEarth[3],OEEarth[4],OEEarth[5],mu)

	#dynamical state of Venus from heliocentric ecliptic system to geocentric ecliptic system:
	StateVenusGeoEclip=StateVenusHelioEclip-StateEarthHelioEclip
	#dynamical state of the Sun in geocentric ecliptic frame (by just changing sign to the state of the Earth in heliocentric ecliptic frame)
	StateSunGeoEclip=-StateEarthHelioEclip
	
	#position vectors (in km) of the Sun and Venus at i-th time instant relative to the geocentric ecliptic reference system
	RVenusGeoEclip=StateVenusGeoEclip[:3]
	RSunGeoEclip=StateSunGeoEclip[:3]

	#mean obliquity (in deg) at i-th time instant:
	epsilon=23.439291-0.0130042*T[i]-0.00059*T[i]**2.+0.001813*T[i]**3.
	#mean obliquity in rad:
	epsilon=epsilon/180.*np.pi

	#rotation matrix from ecliptic to equatorial reference frames:
	R1=np.array([[1.,0.,0.],
		         [0.,np.cos(-epsilon),np.sin(-epsilon)],
		         [0.,-np.sin(-epsilon),np.cos(-epsilon)]])

	#conversion of Cartesian coordinates (in km) from ecliptic geocentric to equatorial geocentric:
	RVenusGeoEquat=np.dot(R1,RVenusGeoEclip)
	RSunGeoEquat=np.dot(R1,RSunGeoEclip)

	#Greenwich Mean Sidereal Time in sec:
	thetag=67310.54841+(876600*3600+8640184.812866)*T[i]+0.093104*T[i]**2.-0.0000062*T[i]**3.
	#conversion of thetag in deg:
	thetag=thetag/240.
	#conversion of thetag in rad:
	thetag=thetag/180.*np.pi

	#rotation matrix from equatorial geocentric to cartesian geographic:
	R3=np.array([[np.cos(thetag),np.sin(thetag),0.],
		         [-np.sin(thetag),np.cos(thetag),0.],
		         [0.,0.,1.]])

	#conversion of Cartesian coordinates (in km) from equatorial geocentric to geographic:
	RVenusGeo=np.dot(R3,RVenusGeoEquat)
	RSunGeo=np.dot(R3,RSunGeoEquat)

	#observer position in Cartesian geographic coordinates (in km):
	RObserverGeo=np.array([Radius*np.cos(latitude)*np.cos(longitude),Radius*np.cos(latitude)*np.sin(longitude),Radius*np.sin(latitude)])

	#position (in km) of the Sun and Venus relative to the observer (in topocentric geographic frame)
	RVenusObsGeo=RVenusGeo-RObserverGeo
	RSunObsGeo=RSunGeo-RObserverGeo

	#rotation matrix from topocentric geographic to topocentric horizontal reference frames:
	Matrix=np.array([[-np.sin(longitude),np.cos(longitude),0.],
		             [-np.sin(latitude)*np.cos(longitude),-np.sin(latitude)*np.sin(longitude),np.cos(latitude)],
		             [np.cos(latitude)*np.cos(longitude),np.cos(latitude)*np.sin(longitude),np.sin(latitude)]])

	#conversion of Cartesian Coordinates (in km) from topocentric geographic to topocentric horizontal:
	RVenusHoriz=np.dot(Matrix,RVenusObsGeo)
	RSunHoriz=np.dot(Matrix,RSunObsGeo)

	#calculation of azimuth A and elevation h in rad:
	AVenus=np.arctan(RVenusHoriz[0]/RVenusHoriz[1])
	hVenus=np.arctan(RVenusHoriz[2]/np.sqrt(RVenusHoriz[0]**2.+RVenusHoriz[1]**2.))
	ASun=np.arctan(RSunHoriz[0]/RSunHoriz[1])
	hSun=np.arctan(RSunHoriz[2]/np.sqrt(RSunHoriz[0]**2.+RSunHoriz[1]**2.))
	#conversion of A and h in deg:
	AVenus=AVenus*180./np.pi
	hVenus=hVenus*180./np.pi
	ASun=ASun*180./np.pi
	hSun=hSun*180./np.pi

	#This definition of the azimuth is ambiguous (and it produces discontinuities), since A is defined in a 360° interval (for example [-180°,180°]), so there always exists a pair of angles (separated by 180°) whose tangent is the same.
	#This is not the case of elevation, since h is defined in the interval [-90°,90°], and in that interval there is a one to one correspondace between angles and tangents.
	#To deal with A ambiguity we just have to consider the sign of the coordinates. The function numpy.arctan returns the angle in the [-90°,90°] interval.
	#If y>=0 then A is in [-90°,90°] (and we don't do nothing). If y<0 then A is outside of [-90°,90°], so if A<=0 we consider A=A+180°, while if A>0 we consider A=A-180°.
	if(RVenusHoriz[1]<0.):
		if(AVenus<=0.):
			AVenus+=180.
		else:
			AVenus-=180.
	if(RSunHoriz[1]<0.):
		if(ASun<=0.):
			ASun+=180.
		else:
			ASun-=180.

	#Azimuth from [-180°,180°] to [0°,360°]
	if(AVenus<0.):
		AVenus+=360.
	if(ASun<0.):
		ASun+=360.

	#do some plots
	plt.scatter(AVenus,hVenus,color='tab:orange',s=0.5)
	plt.scatter(ASun,hSun,color='y',s=0.5)
	if(i==len(JD)-1):
		plt.scatter(AVenus,hVenus,color='tab:orange',s=0.5,label='Venus')
		plt.scatter(ASun,hSun,color='y',s=0.5,label='Sun')
plt.hlines(0.,0.,360.,'k',lw=0.5,label='Horizon')
plt.legend()
plt.xlabel('Azimuth (deg)')
plt.ylabel('Elevation (deg)')
plt.xlim(0.,360.)
plt.ylim(-90.,90.)
plt.show()